# testDevBulko

Voici les consignes demandées :

Vérifications sur le formulaire (JS et/ou PHP):

Adresse e-mail correcte (obligatoire).
Téléphone à 10 chiffres commençant par «0» (obligatoire).
Les champs nom et message sont facultatifs.
Une protection anti injection SQL doit être présente sur l'ensemble des champs.

Traitement du formulaire

Affichage des données brutes sur une seconde page.
Pas d'envoi d'email demandé.