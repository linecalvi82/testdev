<!DOCTYPE html>
<html>
    <head>
        <!-- En-tête de la page -->
        <meta charset="utf-8" />
        <title>Réponse formulaire</title>
    </head>

    <body><?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        echo "Bonjour ".htmlspecialchars($_POST['user_name'])." voici votre adresse email :".htmlspecialchars($_POST['user_mail'])."</br>";
        echo "Le numéro de téléphone est le :".htmlspecialchars($_POST['user_telephone'])." ".htmlspecialchars($_POST['user_message']);
      }
    ?>
    </body>
</html>