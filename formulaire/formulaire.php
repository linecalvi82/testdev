<!DOCTYPE html>
<html>
    <head>
        <!-- En-tête de la page -->
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Formulaire de contact</title>
    </head>

    <body>
      <header>
          <img src="../asset/logoBulko.png" alt="logo bulko" id="logoB" class="logo"/>
          <h1>CONTACT</h1>
          <img src="../asset/reseauxSociaux.png" alt="logo réseaux" id="reseaux" class="logo"/>
      </header>

      <form action="affichageForm.php" method="post" name="form" onsubmit="return verifForm(this)">
        <span id='error'></span>
        <div>
            <input type="text" id="name" name="user_name" placeholder="Nom" class="inputL" onblur="verifNom(this)">
        </div>
        <div>
            <input type="text" id="mail" name="user_mail" placeholder="Mail" class="inputL input23" onblur="verifMail(this)" required>
        </div>
        <div>
            <input type="number" id="telephone" name="user_telephone" placeholder="Telephone" class="inputL input23" maxlength="10" onblur="verifNumero(this)" required>
        </div>
        <div>
          <input type="text" id="message" name="user_message" placeholder="Message" class="inputR" onblur="verifMessage(this)">
        </div>
        <div class="button">
          <button type="submit" id="btn">ENVOYER</button>
        </div>
        <span id='nameError'></span>
      </form>
    </body>

    <script type="text/javascript">

function verifForm(f)
{
   var nameOk = verifNom(f.user_name);
   var mailOk = verifMail(f.user_mail);
   var telOk = verifNumero(f.user_telephone);
   var messageOk = verifMessage(f.user_message);

   var error = document.getElementById('error');

   if(nameOk && mailOk && telOk && messageOk)
      return true;
   else
   {
     if (nameOk != true){
       error.textContent = 'Veuillez remplir correctement le nom';
     }
     if (mailOk != true){
       error.textContent = 'Veuillez remplir correctement l email';
     }
     if (telOk != true){
       error.textContent = 'Veuillez remplir correctement le téléphone';
     }
     if (messageOk != true){
       error.textContent = 'Veuillez remplir correctement le message';
     }
      return false;
   }
}

function verifMail(champ)
{
   var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,3}$/;
   if(!regex.test(champ.value))
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}
function verifNom(champ)
{
  if(champ.value.length == 1 || champ.value.length > 30)
  {
     surligne(champ, true);
     return false;
  }
  else
  {
     surligne(champ, false);
     return true;
  }
}

function verifMessage(champ)
{
  if( champ.value.length == 1)
  {
     surligne(champ, true);
     return false;
  }
  else
  {
     surligne(champ, false);
     return true;
  }

}

function verifNumero(champ)
{
  if(champ.value.length == 10)
  {
    var firstC = champ.value.substr(0,1);
    if(firstC == '0'){
      surligne(champ, false);
      return true;
    }else{
      surligne(champ, true);
      return false;
    }
  }
  else
  {
    surligne(champ, true);
    return false;
  }
}

function surligne(champ, erreur)
{
   if(erreur)
      champ.style.backgroundColor = "#fba";
   else
      champ.style.backgroundColor = "";
}

    </script>
</html>
